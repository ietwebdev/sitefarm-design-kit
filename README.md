# SiteFarm Design Kit

## UCD Marketing Policies and Procedure
UCD Marketing Policies and Procedure: http://marketingtoolbox.ucdavis.edu/start-your-project/policy-procedure.html

All usage of UCD branding must abide by Policy 310-65: http://manuals.ucdavis.edu/ppm/310/310-65.pdf

## Pattern Lab
http://ucd-one-patternlab.s3-website-us-west-1.amazonaws.com
https://bitbucket.org/ietwebdev/sitefarm-pattern-lab-one/overview

See the patterns as HTML in pattern lab or get the code from BitBucket. 

## Style Tile
Style Tile.ai

Illustrator file for create a style tile.
http://styletil.es/

## Starter Pages
Starter Pages.ai

Illustrator file for beginning to use Swatch and Symbol libraries to create wire frames and mockups for SiteFarm sites or custom themes.

## Symbol Libraries (Pattern lab patterns)
https://helpx.adobe.com/illustrator/using/symbols.html

Atoms-Buttons.ai
Atoms-Form-Elements.ai
Atoms-Images.ai
Atoms-Lists.ai
Atoms-Media.ai
Atoms-Tables.ai
Atoms-Text.ai
Molecules-All.ai
Organisms-All.ai
Focal-Link-Icons.ai

The provided Symbol libraries allow you to quickly add elements and components
from SiteFarm's pattern lab. Just load the symbol library you want, and drag and 
drop patterns to the page in Adobe Illustrator.

## Swatch Libraries (Colors)
https://helpx.adobe.com/illustrator/using/using-creating-swatches.html

Swatch Libraries/UC Swatches.ai

The provided Swatch Library allows you to have UC brand colors available as you design.

## Fonts
If you have Adobe CC you can get the fonts via TypeKit
https://typekit.com/fonts/proxima-nova

http://marketingtoolbox.ucdavis.edu/visual-identity/fonts.html

Proxima Nova Regular
Proxima Nova Regular Italic
Proxima Nova Bold
Proxima Nova Extrabold
Proxima Nova Bold Italic

Georgia
Georgia Italic
Georgia Bold

## Vector Patterns
Vector Patterns.ai

This is the file used to create the symbol library and has been included for use as a reference or asset.